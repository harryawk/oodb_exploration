/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package householdAppliance;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.*;

/**
 *
 * @author User
 */
@Entity
public class Branch implements Serializable {
    
    @Id private Integer branchID;
    private String name;
    private boolean isHeadquarters;
    private Integer area;
    
    @OneToOne(mappedBy="addressOfBranch") Location address;
    @ManyToMany(mappedBy="branch") Set<Good> goods;

    public Location getAddress() {
        return address;
    }

    public void setAddress(Location address) {
        this.address = address;
    }

    public Set<Good> getGoods() {
        return goods;
    }

    public void setGoods(Set<Good> goods) {
        this.goods = goods;
    }

    public Branch(Integer branchID, String name, boolean isHeadquarters, Integer area) {
        this.branchID = branchID;
        this.name = name;
        this.isHeadquarters = isHeadquarters;
        this.area = area;
    }

    public Integer getBranchID() {
        return branchID;
    }

    public void setBranchID(Integer branchID) {
        this.branchID = branchID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isIsHeadquarters() {
        return isHeadquarters;
    }

    public void setIsHeadquarters(boolean isHeadquarters) {
        this.isHeadquarters = isHeadquarters;
    }

    public Integer getArea() {
        return area;
    }

    public void setArea(Integer area) {
        this.area = area;
    }
    
    
}
