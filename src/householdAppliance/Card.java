/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package householdAppliance;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

/**
 *
 * @author User
 */
@Entity
public class Card implements Serializable {
    
    @Id private String cardNumber;
    private Integer type;
    private String bank;
    private Date lastDate;
    private Date expDate;
    
    @ManyToOne Customer customer;

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Card(String cardNumber, Integer type, String bank, Date lastDate, Date expDate) {
        this.cardNumber = cardNumber;
        this.type = type;
        this.bank = bank;
        this.lastDate = lastDate;
        this.expDate = expDate;
        this.customer = customer;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public Date getLastDate() {
        return lastDate;
    }

    public void setLastDate(Date lastDate) {
        this.lastDate = lastDate;
    }

    public Date getExpDate() {
        return expDate;
    }

    public void setExpDate(Date expDate) {
        this.expDate = expDate;
    }

    @Override
    public String toString() {
        return "Card{" + "cardNumber=" + cardNumber + ", type=" + type + ", bank=" + bank + ", lastDate=" + lastDate + ", expDate=" + expDate + '}';
    }
    
}
