/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package householdAppliance;

import com.github.javafaker.Faker;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import javax.persistence.*;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author User
 */
public class Main {
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Faker faker = new Faker();
        SimpleDateFormat sdfFaker = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        // TODO code application logic here
        
        // Open a database connection
        // (create a new database if it doesn't exist yet):
        EntityManagerFactory emf =
            Persistence.createEntityManagerFactory("$objectdb/db/household_appliances.odb");
        EntityManager em = emf.createEntityManager();


        Query q1;

        Query query;
        
        ArrayList<Card> arrayOfCard = new ArrayList<>();
        ArrayList<Location> arrayOfLocation = new ArrayList<>();
        ArrayList<Customer> arrayOfCustomer = new ArrayList<>();
        ArrayList<Branch> arrayOfBranch = new ArrayList<>();
        ArrayList<Employee> arrayOfEmployee = new ArrayList<>();
        ArrayList<Good> arrayOfGood = new ArrayList<>();
        ArrayList<Order> arrayOfOrder = new ArrayList<>();
        ArrayList<Regular> arrayOfRegular = new ArrayList<>();
        ArrayList<Executive> arrayOfExecutive = new ArrayList<>();
//        ArrayList<> arrayOfExecutive = new ArrayList<>();
        
        

        ///////////////////////////// Insert data Card
//        em.getTransaction().begin();
        for (int i = 0; i < 20; i++) {
            Set s = new HashSet();
            Integer theID = i;
            Integer theCustomerID = i;
            Card card = new Card(faker.finance().creditCard(), new Integer(0), faker.company().name(), faker.date().future(4, TimeUnit.DAYS), faker.date().future(300, TimeUnit.DAYS));
            Set phoneNumbers = new HashSet();
            for (int j = 0; j < 3; j++) {
                phoneNumbers.add(faker.phoneNumber().cellPhone());
            }
            Employee employee = new Employee(theID, faker.name().fullName(), phoneNumbers, faker.date().past(300, TimeUnit.DAYS), faker.date().birthday(23, 40), new Integer(faker.random().nextInt(15)));
//            Customer customer;
            System.out.println(employee);
            Customer customer = new Customer(theID, faker.name().fullName(), phoneNumbers, faker.date().birthday(30, 50));
            if (i % 2 == 0) {
                customer = new Regular(customer, new Integer((int) faker.number().randomNumber()));
                
                System.out.println(customer);
                arrayOfRegular.add((Regular) customer);
            } else {
                customer = new Executive(customer);
                employee.setCustomer(customer);
                Location employeeLocation = new Location(faker.address().streetAddress(), faker.address().cityName(), faker.address().state(), faker.address().zipCode());
                employee.setAddress(employeeLocation);
                arrayOfLocation.add(employeeLocation);
                arrayOfEmployee.add(employee);
                arrayOfExecutive.add((Executive) customer);
//                em.persist(employeeLocation);
//                em.persist(employee);
            }
            Location customerLocation = new Location(faker.address().streetAddress(), faker.address().cityName(), faker.address().state(), faker.address().zipCode());
            s.add(card);
            customer.setCard(s);
            customer.setAddress(customerLocation);
            arrayOfCard.add(card);
            arrayOfCustomer.add(customer);
            arrayOfLocation.add(customerLocation);
        }
//        em.getTransaction().commit();
        
//        em.getTransaction().begin();
        
        for (int i = 0; i < 10; i++) {
            Branch branch = new Branch(new Integer(i), faker.company().name(), faker.random().nextBoolean(), faker.number().numberBetween(100, 200));
            
            Set<Good> goods = new HashSet();
            for (int j = 0; j < 4; j++) {
                Good good = new Good(new Integer(i), faker.lorem().sentence(), faker.color().name(), faker.random().nextInt(i+1), faker.random().toString(), faker.random().nextInt(i+1));
                goods.add(good);
                arrayOfGood.add(good);
//                em.persist(good);
            }
            Order order = new Order(faker.random().toString(), faker.date().past(30, TimeUnit.DAYS));
            order.setOrders(goods);
            order.setOrderedBy(arrayOfCustomer.get(i));
            arrayOfOrder.add(order);
//            em.persist(order);
            branch.setGoods(goods);
            Location branchLocation = new Location(faker.address().streetAddress(), faker.address().cityName(), faker.address().state(), faker.address().zipCode());
            branch.setAddress(branchLocation);
            arrayOfBranch.add(branch);
//            em.persist(branch);
        }
        
//        Commit array of cards
        em.getTransaction().begin();
            for (int i = 0; i < arrayOfCard.size(); i++) {
                em.persist(arrayOfCard.get(i));
            }
        em.getTransaction().commit();
        
//        Commit array of cards
        em.getTransaction().begin();
            for (int i = 0; i < arrayOfLocation.size(); i++) {
                em.persist(arrayOfLocation.get(i));
            }
        em.getTransaction().commit();
//        Commit array of cards
        System.out.println(arrayOfCustomer);
        em.getTransaction().begin();
            for (int i = 0; i < arrayOfCustomer.size(); i++) {
                em.persist(arrayOfCustomer.get(i));
            }
        em.getTransaction().commit();
        
//        Commit array of cards
        em.getTransaction().begin();
            for (int i = 0; i < arrayOfExecutive.size(); i++) {
                em.persist(arrayOfExecutive.get(i));
            }
        em.getTransaction().commit();
        
//        Commit array of cards
        em.getTransaction().begin();
            for (int i = 0; i < arrayOfEmployee.size(); i++) {
                em.persist(arrayOfEmployee.get(i));
            }
        em.getTransaction().commit();
//        
//        Commit array of cards
        em.getTransaction().begin();
            for (int i = 0; i < arrayOfRegular.size(); i++) {
                em.persist(arrayOfRegular.get(i));
            }
        em.getTransaction().commit();
//        
//        Commit array of cards
        em.getTransaction().begin();
            for (int i = 0; i < arrayOfOrder.size(); i++) {
                em.persist(arrayOfOrder.get(i));
            }
        em.getTransaction().commit();
        
//        Commit array of 
        em.getTransaction().begin();
            for (int i = 0; i < arrayOfBranch.size(); i++) {
                em.persist(arrayOfBranch.get(i));
            }
        em.getTransaction().commit();
        
        
//        Commit array of good
        em.getTransaction().begin();
            for (int i = 0; i < arrayOfGood.size(); i++) {
                em.persist(arrayOfGood.get(i));
            }
        em.getTransaction().commit();

        // Close the database connection:
        em.close();
        emf.close();
        
    }
    
}
