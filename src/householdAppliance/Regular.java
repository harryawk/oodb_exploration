/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package householdAppliance;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import javax.persistence.*;

/**
 *
 * @author User
 */
@Entity
public class Regular extends Customer implements Serializable {
    
    private Integer points;
    
    public Regular(Customer customer, Integer points) {
        super(customer.getCustomerID(), customer.getName(), customer.getPhoneNumbers(), customer.getBirthDate());
        this.points = points;
    }

    public Integer getPoints() {
        return points;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }
    
}
