/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package householdAppliance;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import javax.persistence.*;

/**
 *
 * @author User
 */
@Entity
public class Employee implements Serializable {
    
    @Id private Integer employeeID;
    private String name;
    private Set<String> phoneNumbers;
    private Date startDate;
    private Date birthDate;
    private Integer baseSalary;
    
    @OneToOne Customer customer;
    @OneToOne Location address;

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    @Override
    public String toString() {
        return "Employee{" + "employeeID=" + employeeID + ", name=" + name + ", phoneNumbers=" + phoneNumbers + ", startDate=" + startDate + ", birthDate=" + birthDate + ", baseSalary=" + baseSalary + ", customer=" + customer + ", address=" + address + '}';
    }

    public Location getAddress() {
        return address;
    }

    public void setAddress(Location address) {
        this.address = address;
    }
    

    public Employee(Integer employeeID, String name, Set<String> phoneNumbers, Date startDate, Date birthDate, Integer baseSalary) {
        this.employeeID = employeeID;
        this.name = name;
        this.phoneNumbers = phoneNumbers;
        this.startDate = startDate;
        this.birthDate = birthDate;
        this.baseSalary = baseSalary;
    }

    public Integer getEmployeeID() {
        return employeeID;
    }

    public void setEmployeeID(Integer employeeID) {
        this.employeeID = employeeID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<String> getPhoneNumbers() {
        return phoneNumbers;
    }

    public void setPhoneNumbers(Set<String> phoneNumbers) {
        this.phoneNumbers = phoneNumbers;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public Integer getBaseSalary() {
        return baseSalary;
    }

    public void setBaseSalary(Integer baseSalary) {
        this.baseSalary = baseSalary;
    }
    
    
}
