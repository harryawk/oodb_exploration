/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package householdAppliance;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import javax.persistence.*;

/**
 *
 * @author User
 */
@Entity
public class Executive extends Customer implements Serializable {
    
    @OneToOne(mappedBy="customer") Employee employee;
    
    public Executive(Customer customer) {
        super(customer.getCustomerID(), customer.getName(), customer.getPhoneNumbers(), customer.getBirthDate());
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }    
    
}
