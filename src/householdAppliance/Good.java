/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package householdAppliance;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.*;

/**
 *
 * @author User
 */
@Entity
public class Good implements Serializable {
    
    @Id private Integer goodID;
    private String description;
    private String color;
    private Integer size;
    private String type;
    private Integer quantity;
    
    @OneToMany(mappedBy="product") Set<Good> components;
    @ManyToOne Good product;
    @ManyToMany Set<Branch> branch;
    @ManyToMany(mappedBy="goods") Set<Order> orders;

    public Set<Good> getComponents() {
        return components;
    }

    public void setComponents(Set<Good> components) {
        this.components = components;
    }

    public Good getProduct() {
        return product;
    }

    public void setProduct(Good product) {
        this.product = product;
    }

    public Set<Branch> getBranch() {
        return branch;
    }

    public void setBranch(Set<Branch> branch) {
        this.branch = branch;
    }

    public Set<Order> getOrders() {
        return orders;
    }

    public void setOrders(Set<Order> orders) {
        this.orders = orders;
    }
    

    
    
    public Good(Integer goodID, String description, String color, Integer size, String type, Integer quantity) {
        this.goodID = goodID;
        this.description = description;
        this.color = color;
        this.size = size;
        this.type = type;
        this.quantity = quantity;
    }

    public Integer getGoodID() {
        return goodID;
    }

    public void setGoodID(Integer goodID) {
        this.goodID = goodID;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }
    
    
    
}
