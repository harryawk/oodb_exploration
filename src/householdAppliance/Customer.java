/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package householdAppliance;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import javax.persistence.*;

/**
 *
 * @author User
 */
@Entity
public class Customer implements Serializable {

//    @Id @GeneratedValue
//    private Long id;
    @Id
    protected Integer customerID;
    
    protected String name;
    protected Set<String> phoneNumbers;
    protected Date birthDate;
    
    @OneToMany(mappedBy="customer") Set<Card> card;
    @OneToMany(mappedBy="orderedBy") Set<Order> orders;
    @OneToOne Location address;

    public Set<Card> getCard() {
        return card;
    }

    public void setCard(Set<Card> card) {
        this.card = card;
    }

    public Set<Order> getOrders() {
        return orders;
    }

    public void setOrders(Set<Order> orders) {
        this.orders = orders;
    }

    public Location getAddress() {
        return address;
    }

    public void setAddress(Location address) {
        this.address = address;
    }

    

    public Customer(Integer customerID, String name, Set<String> phoneNumbers, Date birthDate) {
        this.customerID = customerID;
        this.name = name;
        this.phoneNumbers = phoneNumbers;
        this.birthDate = birthDate;
    }

    public Integer getCustomerID() {
        return customerID;
    }

    public void setCustomerID(Integer customerID) {
        this.customerID = customerID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<String> getPhoneNumbers() {
        return phoneNumbers;
    }

    public void setPhoneNumbers(Set<String> phoneNumbers) {
        this.phoneNumbers = phoneNumbers;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

//    public void setId(Long id) {
//        this.id = id;
//    }

    @Override
    public String toString() {
        return "Customer{" + "customerID=" + customerID + ", name=" + name + ", phoneNumbers=" + phoneNumbers + ", birthDate=" + birthDate + '}';
    }
    
    
    
}
