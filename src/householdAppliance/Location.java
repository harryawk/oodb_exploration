/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package householdAppliance;

import java.io.Serializable;
import javax.persistence.*;

/**
 *
 * @author User
 */
@Entity
public class Location implements Serializable {
    
    @Id @GeneratedValue
    private Long id;
    
    private String address;
    private String city;
    private String province;
    private String postcode; 
    
    public Location(String address, String city, String province, String postcode) {
        this.address = address;
        this.city = city;
        this.province = province;
        this.postcode = postcode;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }
    
    
    
}
